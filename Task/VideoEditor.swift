//
//  VideoEditor.swift
//  Task
//
//  Created by khalifa on 8/31/20.
//  Copyright © 2020 Khalifa. All rights reserved.
//

import Foundation
import AVKit
import AVFoundation

class VideoEditor {
    typealias CompletionHandler = (URL?,_ errorMessage: String?) -> Void
    
    func limitVideoLength(sourceURL: URL, maxLengthInSeconds: Float, completion: @escaping CompletionHandler) {
        let fileManager = FileManager.default
        let documentDirectory = fileManager.urls(for: .documentDirectory, in: .userDomainMask)[0]
        let asset = AVAsset(url: sourceURL)
        let length = Float(asset.duration.value) / Float(asset.duration.timescale)
        guard length > maxLengthInSeconds else {
            completion(sourceURL, nil)
            return
        }
        guard let outputURL = createNewOutputURL(fileManager: fileManager, sourceURL: sourceURL, documentDirectory: documentDirectory) else {
            completion(nil, "Failed to create directory")
            return
        }
        guard let exportSession = createExportSession(outputURL: outputURL, asset: asset, maxLengthInSeconds: maxLengthInSeconds) else {
            completion(nil, "Failed")
            return
        }
        exportSession.exportAsynchronously {
            self.handleExportSessionCompletion(exportSession: exportSession, outputURL: outputURL, completion: completion)
        }
    }
    
    private func createNewOutputURL(fileManager: FileManager, sourceURL: URL, documentDirectory: URL)-> URL? {
        var outputURL = documentDirectory.appendingPathComponent("output")
        do {
            try fileManager.createDirectory(at: outputURL, withIntermediateDirectories: true, attributes: nil)
            outputURL = outputURL.appendingPathComponent("\(sourceURL.lastPathComponent).mp4")
        } catch let _ {
            return nil
        }
        try? fileManager.removeItem(at: outputURL)
        return outputURL
    }
    
    private func createExportSession(outputURL: URL, asset: AVAsset, maxLengthInSeconds: Float)-> AVAssetExportSession? {
        guard let exportSession = AVAssetExportSession(asset: asset, presetName: AVAssetExportPresetHighestQuality) else { return nil }
        exportSession.outputURL = outputURL
        exportSession.outputFileType = .mp4
        let timeRange = CMTimeRange(start: CMTime(seconds: 0, preferredTimescale: 1000),
                                    end: CMTime(seconds: Double(maxLengthInSeconds), preferredTimescale: 1000))
        exportSession.timeRange = timeRange
        return exportSession
    }
    
    private func handleExportSessionCompletion(exportSession: AVAssetExportSession, outputURL: URL, completion: @escaping CompletionHandler) {
        DispatchQueue.main.async {
            switch exportSession.status {
            case .completed:
                completion(outputURL, nil)
            case .failed:
                completion(nil, "Failed \(exportSession.error?.localizedDescription ?? "")")
            case .cancelled:
                completion(nil, "Cancelled \(exportSession.error?.localizedDescription ?? "")")
            default: break
            }
        }
        
    }
    
}
