//
//  ViewController.swift
//  Task
//
//  Created by khalifa on 8/31/20.
//  Copyright © 2020 Khalifa. All rights reserved.
//
import UIKit
import AVKit
import AVFoundation
import UIKit

protocol HomeView: AnyObject {
    func playVideo(_ url: URL)
    func startSelectVideoView()
    func showLoadingView()
    func hideLoadingView()
    func showError(_ message: String)
    func showSelectVideoButton()
    func hideSelectVideoButton()
}

class ViewController: UIViewController {
    @IBOutlet var loadingIndicator: UIActivityIndicatorView! {
        didSet {
            loadingIndicator.hidesWhenStopped = true
        }
    }
    @IBOutlet var selectVideoButton: UIButton!
    
    //Should be injected
    private lazy var presenter: HomePresenterProtocol? = {
        return HomePresenter(view: self)
    }()
    
    @IBAction func didClickSelectVideo() {
        presenter?.didClickSelectVideo()
    }
}

extension ViewController: HomeView {
    func showSelectVideoButton() {
        selectVideoButton.isHidden = false
    }
    
    func hideSelectVideoButton() {
        selectVideoButton.isHidden = true
    }
    
    func showLoadingView() {
        loadingIndicator.startAnimating()
    }
    
    func hideLoadingView() {
        loadingIndicator.stopAnimating()
        
    }
    
    func showError(_ message: String) {
        let alert = UIAlertController(title:"", message: message, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    func startSelectVideoView() {
        let picker = UIImagePickerController()
        picker.delegate = self
        picker.sourceType = .savedPhotosAlbum
        picker.mediaTypes = ["public.movie"]
        picker.allowsEditing = false
        present(picker, animated: true, completion: nil)
    }
    
    func playVideo(_ url: URL) {
        let player = AVPlayer(url: url)
        let viewController = AVPlayerViewController()
        viewController.player = player
        self.present(viewController, animated: true) { viewController.player?.play() }
    }
}


extension ViewController: (UIImagePickerControllerDelegate & UINavigationControllerDelegate) {
    func imagePickerController (_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey: Any]) {
        guard let videoURL = info[UIImagePickerController.InfoKey.mediaURL] as? NSURL else { return }
        presenter?.didSelectVideo(videoURL as URL)
        self.dismiss(animated: true, completion: nil)
    }
}
