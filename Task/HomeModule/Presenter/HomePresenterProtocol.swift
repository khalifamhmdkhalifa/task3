//
//  HomePresenterProtocol.swift
//  Task
//
//  Created by khalifa on 8/31/20.
//  Copyright © 2020 Khalifa. All rights reserved.
//

import Foundation

protocol HomePresenterProtocol {
    func didClickSelectVideo()
    func didSelectVideo(_ url: URL)
}
