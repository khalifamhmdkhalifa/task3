//
//  HomePresenter.swift
//  Task
//
//  Created by khalifa on 8/31/20.
//  Copyright © 2020 Khalifa. All rights reserved.
//

import Foundation

class HomePresenter {
    private weak var view: HomeView?
    private static let maxVideoLength: Float = 30
    
    init(view: HomeView) {
        self.view = view
    }
}

extension HomePresenter: HomePresenterProtocol {
    func didClickSelectVideo() {
        view?.startSelectVideoView()
    }
    
    func didSelectVideo(_ url: URL) {
        view?.showLoadingView()
        view?.hideSelectVideoButton()
        VideoEditor().limitVideoLength(sourceURL: url as URL,maxLengthInSeconds: HomePresenter.maxVideoLength) { [weak self] url, error in
            guard let self = self else { return }
            self.view?.hideLoadingView()
            self.view?.showSelectVideoButton()
            if let url = url {
                self.view?.playVideo(url as URL)
            } else if let error = error {
                self.view?.showError(error)
            }
        }
    }
}

